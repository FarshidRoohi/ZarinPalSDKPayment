Pod::Spec.new do |s|
s.name             = 'ZarinPalSDKPayment'
s.version          = '0.2.4'
s.summary          = 'ZarinPal SDK Payment'
s.description      = 'ZarinPal SDK Payment | Payment Request and Automatic Verification in iOS Clinet'
s.homepage         = 'http://zarinpal.com/'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'ZarinPal' => 'developers@zarinpal.com' }
s.platform         = :ios, '11.3'
s.source           = { :git => 'https://gitlab.com/FarshidRoohi/ZarinPalSDKPayment.git', :tag => '0.2.4' }
s.ios.deployment_target = '10.0'
#s.ios.vendored_frameworks = 'ZarinPalSDKPayment.framework'

#s.source_files = 'ZarinPalSDKPayment'
s.source_files = 'ZarinPalSDKPayment/*.{swift,h}'
#s.resource_bundle = {'ZarinPalSDKPayment' => 'ZarinPalSDKPayment/**/*.{storyboard}'}
s.requires_arc = true
s.ios.frameworks = 'UIKit', 'Foundation'
s.resource = 'ZarinPalSDKPayment/PaymentBoard.storyboard'
end
